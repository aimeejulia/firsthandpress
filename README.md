# First Hand Press is an open access, eco-friendly, open publishing house that focuses on representing the underrepresented and presents first hand accounts on life and its' rainbow coloured topics.

## What kinds of books /projects do we publish?

First hand press focuses on non fiction books covering all aspects of life directly reported from the people.

### What is a project?
A project is a book / collection of books. You can look at our list of projects here.

## How is it Open Access?

Free unfettered online open access to all our projects, now and forever.

## What about copyright?

You can have a look at our chosen licence [here](../blob/master/LICENSE).

## How are we eco friendly?
Our printed books /projects are:
100% biodegradable,
Printed with eco friendly ink
Printed on recycled paper / paper made from sustainable sources
Printed and distributed as locally as possible

## Our website and data are:
stored on eco friendly carbon neutral servers

## What is an open company?

### Open accounting & financial model
We want to make our company transparent, this means that all costs, income and distributions of profits are made public.

### Open policies
All company policies will be publicly available.

## Who are the people behind First Hand Publishers?
Get to know our [community](https://gitlab.com/aimeejulia/firsthandpress/wikis/About-First-Hand-Press).

### Who / what is a contributor?
Contributors are people whose materials are published in the projects.

### How do I become a contributor?
You can check out our in progress projects and see which topic you can contribute to.

## What about money...

### I want to work for you - how much are employees paid?
First Hand Press does not have any employees instead it is run and maintained by collaborators.

All collaborators receive 1 contributor share in the current in progress project/s.

You can see the collaborators current shares & projects here.

### I like this project and would like to volunteer my time & skills - how do I do that?

That's awesome, you can join the discussion on our gitlab repo and our freenode irc channel #firsthand.press

### How are contributors compensated?
Contributors receive an annual payment for profit made in the project that they have participated in. For more details on how profit is calculated you can look at our Financial Model.

### This is an amazing project and I would like to support it - how do I do that?

Great! You can do so in many ways...

* Donate money through [Librepay](https://liberapay.com/for/firsthand.press)
* Share our projects with your contacts
* Volunteer your time and skills
* Buy copies of our projects for your loved ones
* Have any ideas on how you can help but don't find it here? No problem - add it on the discussion board :-)
